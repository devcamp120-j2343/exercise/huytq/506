const express = require("express");

const courseMiddleware = require("../middlewares/course.middlewares");

const router = express.Router();

router.use((req, res, next) => {
    console.log("Request URL course: ", req.url);

    next();
});

router.get("/", courseMiddleware.getAllCourseMiddleware,(req, res) => {
    res.json({
        message: "Get all courses"
    })
})

router.post("/", courseMiddleware.createCourseMiddleware, (req, res) => {
    res.json({
        message: "Create course"
    })
});

router.get("/:courseId", courseMiddleware.getDetailCourseMiddleware, (req, res) => {
    var courseId = req.params.courseId;

    res.json({
        message: "Get course id = " + courseId
    })
});

router.put("/:courseId", courseMiddleware.updateCourseMiddleware, (req, res) => {
    var courseId = req.params.courseId;

    res.json({
        message: "Update course id = " + courseId
    })
});

router.delete("/:courseId", courseMiddleware.deleteCourseMiddleware, (req, res) => {
    var courseId = req.params.courseId;

    res.json({
        message: "Delete course id = " + courseId
    })
});

module.exports = router;
