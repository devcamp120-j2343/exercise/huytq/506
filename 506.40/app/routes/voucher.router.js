const express = require("express");

const voucherMiddleware = require("../middlewares/voucher.middlewares");

const router = express.Router();
const vVOUCHER_URL = "http://203.171.20.210:8080/devcamp-pizza365/voucher_detail/";
router.use((req, res, next) => {
    console.log("Request URL voucher: ", req.url);

    next();
});

router.get("/", voucherMiddleware.getAllVoucherMiddleware,(req, res) => {
    res.json({
        message: "thông tin api voucher: " + vVOUCHER_URL
    })
})

router.post("/", voucherMiddleware.createVoucherMiddleware, (req, res) => {
    res.json({
        message: "Create voucher from api: " + vVOUCHER_URL
    })
});

router.get("/:voucherId", voucherMiddleware.getDetailVoucherMiddleware, (req, res) => {
    var voucherId = req.params.voucherId;

    res.json({
        message: "Get voucher id = " + voucherId
    })
});

router.put("/:voucherId", voucherMiddleware.updateVoucherMiddleware, (req, res) => {
    var voucherId = req.params.voucherId;

    res.json({
        message: "Update voucher id = " + voucherId    })
});

router.delete("/:voucherId", voucherMiddleware.deleteVoucherMiddleware, (req, res) => {
    var voucherId = req.params.voucherId;

    res.json({
        message: "Delete voucher id = " + voucherId
    })
});

module.exports = router;
