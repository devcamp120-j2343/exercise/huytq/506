//Import thư viện ExpressJS
// import express from 'express';
const express = require('express');

// Khởi tạo app nodejs bằng express
const app = express();

// Khai báo ứng dụng sẽ chạy trên cổng 8000
const port = 8000;

// Khai báo ứng dụng đọc được body raw json (Build in middleware)
app.use(express.json());


//Middleware in ra console thời gian hiện tại mỗi khi API gọi
app.use((request, response, next) => {
    var today = new Date();

    console.log("Current time: ", today);

    next();
});

//Middleware in ra console request method mỗi khi API gọi
app.use((request, response, next) => {
    console.log("Method: ", request.method);

    next();
});

// Khai báo API trả về chuỗi
app.get("/", (request, response) => {
    //Viết code xử lý trong đây
    var today = new Date();

    var resultString = `Xin chào, hôm nay là ngày ${today.getDate()} tháng ${today.getMonth() + 1} năm ${today.getFullYear()}`;

    response.json({
        result: resultString
    });
});

// Tạo GET API
app.get("/get-method", (request, response) => {
    response.json({
        method: "GET"
    })
})

// Tạo POST API
app.post("/post-method", (request, response) => {
    response.json({
        method: "POST"
    })
})

// Tạo PUT API
app.put("/put-method", (request, response) => {
    response.json({
        method: "PUT"
    })
})

// Tạo DELETE API
app.delete("/delete-method", (request, response) => {
    response.json({
        method: "DELETE"
    })
})

//Tạo API lấy request params
app.get("/get-params/:param1/:param2", (request, response) => {
    var param1 = request.params.param1;
    var param2 = request.params.param2;
    //Thao tác dữ liệu từ param1
    response.json({
        param1: param1,
        param2: param2,
    })
})

//Tạo API lấy request query
app.get("/get-query", (request, response) => {
    var query = request.query;

    //Validate..

    response.json({
        query: query
    });
})

//Tạo API lấy request body raw json
app.post("/get-body-raw", (request, response) => {
    var body = request.body;

    //Validate

    response.json({
        body: body
    })
})

// Chạy ứng dụng
app.listen(port, () => {
    console.log("Ứng dụng chạy trên cổng: ", port);
})

