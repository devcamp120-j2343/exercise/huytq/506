const express = require("express");

const orderMiddleware = require("../middlewares/order.middlewares");

const router = express.Router();
const vORDER_URL = "http://203.171.20.210:8080/devcamp-pizza365/orders";
router.use((req, res, next) => {
    console.log("Request URL order: ", req.url);

    next();
});

router.get("/",orderMiddleware.getAllOrderMiddleware,(req, res) => {
    res.json({
        message: "thông tin api order: " + vORDER_URL
    })
})

router.post("/", orderMiddleware.createOrderMiddleware, (req, res) => {
    res.json({
        message: "Create order from api: " + vORDER_URL
    })
});

router.get("/:orderId", orderMiddleware.getDetailOrderMiddleware, (req, res) => {
    var orderId = req.params.orderId;

    res.json({
        message: "Get order id = " + orderId
    })
});

router.put("/:orderId", orderMiddleware.updateOrderMiddleware, (req, res) => {
    var orderId = req.params.orderId;

    res.json({
        message: "Update order id = " + orderId    })
});

router.delete("/:orderId", orderMiddleware.deleteOrderMiddleware, (req, res) => {
    var orderId = req.params.orderId;

    res.json({
        message: "Delete order id = " + orderId
    })
});

module.exports = router;
