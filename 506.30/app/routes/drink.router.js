const express = require("express");

const drinkMiddleware = require("../middlewares/drink.middlewares");

const router = express.Router();
const vDRIK_URL ="http://203.171.20.210:8080/devcamp-pizza365/drinks";
router.use((req, res, next) => {
    console.log("Request URL drink: ", req.url);

    next();
});

router.get("/", drinkMiddleware.getAllDrinkMiddleware,(req, res) => {
    res.json({
        message: "thông tin api drink: " + vDRIK_URL
    })
})

router.post("/", drinkMiddleware.createDrinkMiddleware, (req, res) => {
    res.json({
        message: "Create drink from api: " + vDRIK_URL
    })
});

router.get("/:drinkId", drinkMiddleware.getDetailDrinkMiddleware, (req, res) => {
    var drinkId = req.params.drinkId;

    res.json({
        message: "Get drink id = " + drinkId
    })
});

router.put("/:drinkId", drinkMiddleware.updateDrinkMiddleware, (req, res) => {
    var drinkId = req.params.drinkId;

    res.json({
        message: "Update drink id = " + drinkId
    })
});

router.delete("/:drinkId", drinkMiddleware.deleteDrinkMiddleware, (req, res) => {
    var drinkId = req.params.drinkId;

    res.json({
        message: "Delete drink id = " + drinkId
    })
});

module.exports = router;
