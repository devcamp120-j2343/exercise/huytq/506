const express = require("express");

const userMiddleware = require("../middlewares/user.middlewares");

const router = express.Router();
const vUSER_URL = "http://203.171.20.210:8080/crud-api/users/";
router.use((req, res, next) => {
    console.log("Request URL user: ", req.url);

    next();
});

router.get("/", userMiddleware.getAllUserMiddleware,(req, res) => {
    res.json({
        message: "thông tin api user: " + vUSER_URL
    })
})

router.post("/", userMiddleware.createUserMiddleware, (req, res) => {
    res.json({
        message: "Create user from api: " + vUSER_URL
    })
});

router.get("/:userId", userMiddleware.getDetailUserMiddleware, (req, res) => {
    var userId = req.params.userId;

    res.json({
        message: "Get user id = " + userId
    })
});

router.put("/:userId", userMiddleware.updateUserMiddleware, (req, res) => {
    var userId = req.params.userId;

    res.json({
        message: "Update user id = " + userId    })
});

router.delete("/:userId", userMiddleware.deleteUserMiddleware, (req, res) => {
    var userId = req.params.userId;

    res.json({
        message: "Delete user id = " + userId
    })
});

module.exports = router;
